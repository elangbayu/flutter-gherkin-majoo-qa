import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  group('Facebook Clone', () {
    final buttonLogin = find.byValueKey('loginButton');
    final emailInput = find.byValueKey('inputEmail');
    final passwordInput = find.byValueKey('inputPassword');
    final fieldName = find.byValueKey('namaField');
    final fieldEmail = find.byValueKey('emailField');

    FlutterDriver driver;

    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
      if (driver != null){
        driver.close();
      }
    });

    test('Login', () async {
      await driver.tap(emailInput);
      await driver.enterText("elangbayu17@gmail.com");
      await driver.tap(passwordInput);
      await driver.enterText("blablabla");
      
      await driver.tap(buttonLogin);

      expect(await driver.getText(fieldName), "Zonen GAJAYANA");
      expect(await driver.getText(fieldEmail), "zonen@getnada.com");
    });
  });
}