import 'dart:io';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../pages/profile_page.dart';

class SeeProfile extends ThenWithWorld<FlutterWorld> {
  SeeProfile()
      : super(StepDefinitionConfiguration()..timeout = Duration(seconds: 10));

  @override
  Future<void> executeStep() async {
    ProfilePage profilePage = ProfilePage(world.driver);

    expectMatch(await profilePage.getName(), 'Zonen GAJAYANA');
    expectMatch(await profilePage.getEmail(), 'zonen@getnada.com');
  }

  @override
  RegExp get pattern =>
      RegExp(r"I can see my name and email");
}
