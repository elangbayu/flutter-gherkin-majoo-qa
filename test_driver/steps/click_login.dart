import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../pages/login_page.dart';

class ClickLogin extends AndWithWorld<FlutterWorld>{
  ClickLogin()
    : super(StepDefinitionConfiguration()..timeout = Duration(seconds: 10));

  @override
  Future<void> executeStep() async {
    LoginPage loginPage = LoginPage(world.driver);

    await loginPage.clickButtonLogin();

  }

  @override
  RegExp get pattern => RegExp(r"I click the login button");

}