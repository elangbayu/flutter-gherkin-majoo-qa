import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';
import 'package:gherkin/gherkin.dart';

import '../pages/login_page.dart';

class InsertEmailPassword extends Given2WithWorld<String, String, FlutterWorld>{
  InsertEmailPassword()
    : super(StepDefinitionConfiguration()..timeout = Duration(seconds: 10));

  @override
  Future<void> executeStep(String email, String password) async {
    LoginPage loginPage = LoginPage(world.driver);

    await loginPage.inputEmail(email);
    await loginPage.inputPassword(password);

  }

  @override
  RegExp get pattern => RegExp(r"I insert email {string} and password {string}");

}