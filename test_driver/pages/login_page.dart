import 'package:flutter_driver/flutter_driver.dart';

class LoginPage {
  final buttonLogin = find.byValueKey('loginButton');
  final emailInput = find.byValueKey('inputEmail');
  final passwordInput = find.byValueKey('inputPassword');

  FlutterDriver _driver;

  LoginPage(FlutterDriver driver){
    this._driver = driver;
  }

  Future<void> inputEmail(String email) async {
    await _driver.tap(emailInput);
    return await _driver.enterText(email);
  }

  Future<void> inputPassword(String password) async {
    await _driver.tap(passwordInput);
    return await _driver.enterText(password);
  }

  Future<void> clickButtonLogin() async {
    return await _driver.tap(buttonLogin);
  }

}