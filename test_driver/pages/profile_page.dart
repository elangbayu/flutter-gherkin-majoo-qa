import 'package:flutter_driver/flutter_driver.dart';

class ProfilePage {
  final fieldName = find.byValueKey('namaField');
  final fieldEmail = find.byValueKey('emailField');

  FlutterDriver _driver;

  ProfilePage(FlutterDriver driver){
    this._driver = driver;
  }

  Future<String> getName() async {
    return await _driver.getText(fieldName);
  }

  Future<String> getEmail() async {
    return await _driver.getText(fieldEmail);
  }
  
}